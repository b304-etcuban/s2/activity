package com.zuitt.WDC043_S2_A2;

import java.util.Arrays;
import java.util.HashMap;
import java.util.ArrayList;

public class PrimeNumber {
    public static void main(String[] args) {
        int[] primes = new int[5];
        primes[0] = 2;
        primes[1] = 3;
        primes[2] = 5;
        primes[3] = 7;
        primes[4] = 11;

        System.out.println("The first prime number is " + primes[0]);

        ArrayList<String> students = new ArrayList<String>(Arrays.asList("John", "Jane", "Chloe", "Zoey"));
        System.out.println("My friends are: " + students);

        HashMap<String, String> inventory = new HashMap<String, String>(){
            {
                put("toothpaste", "15");
                put("toothbrush", "20");
                put("soap", "12");
            }
        };
        System.out.println("Our current inventory consists of: " + inventory);


    }
}
